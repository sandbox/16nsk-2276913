<?php

/**
 * @file
 * Field validation CIF validator.
 */

$plugin = array(
  'label' => t('CIF'),
  'description' => t('Validates CIF using PHP-CIF library.'),
  'handler' => array('class' => 'field_validation_cif_validator'),
);

class field_validation_cif_validator extends field_validation_validator {

  /**
   * Validate field. 
   */
  public function validate() {
    // Load library
    if (($library = libraries_load('php-cif')) && !empty($library['loaded'])) {
      // Verific valoare goală
      if(!empty($this->value)) {
        // Verify value
        if (!validateCIF($this->value)) {
          $this->set_error();
        }
      }
    }
    else {
      // No library no validation
      $this->set_error();
    }
  }
}
